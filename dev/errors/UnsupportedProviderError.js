export default class UnsupportedProviderError extends Error {
    constructor(expected, given) {
        let givenStatement = given ? `but was given ${given}` : '';
        super(`Expected provider of type ${expected} ${givenStatement}`);
    }
}