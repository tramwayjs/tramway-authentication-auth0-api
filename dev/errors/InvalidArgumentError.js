export default class InvalidArgumentError extends Error {
    constructor(args) {
        super(`The following arguments needs to be set: ${args.join(', ')}`);
    }
}