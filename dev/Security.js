import {Authentication, strategies} from 'tramway-core-router';
let {AuthenticationStrategy} = strategies;

export default class Security {
    constructor(authenticationStrategy) {
        let authentication = new Authentication(authenticationStrategy);
        return authentication.check();
    }
}