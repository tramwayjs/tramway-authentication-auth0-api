import {AccessTokenPolicy, ProperScopePolicy, CompositePolicy} from './policies';
import {JwtProvider, ScopesProvider} from './provider';

export default class PolicyFactory {
    constructor(options) {
        const jwtProvider = new JwtProvider(options);
        this.accessTokenPolicy = new AccessTokenPolicy(jwtProvider);
    }

    buildBasic() {
        return this.accessTokenPolicy;
    }

    buildScoped(scopes) {
        let policy = new CompositePolicy([this.accessTokenPolicy]);
        const scopesProvider = new ScopesProvider(scopes);
        return policy.add(new ProperScopePolicy(scopesProvider));
    }
}
