import { policies } from 'tramway-core-router';
let { AuthenticationStrategy } = policies;

import {JwtProvider} from '../provider';
import {UnsupportedProviderError} from '../errors';

export default class AccessTokenPolicy extends AuthenticationStrategy {
    constructor(jwtProvider) {
        super(null);
        if (!(jwtProvider instanceof JwtProvider)) {
            throw new UnsupportedProviderError(JwtProvider.name)
        }
        this.jwtProvider = jwtProvider;
    }

    check(cb) {
        return this.jwtProvider.checkJwt;
    }

}