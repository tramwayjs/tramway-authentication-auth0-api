import { policies } from 'tramway-core-router';
let { AuthenticationStrategy } = policies;

import {ScopesProvider} from '../provider';
import {UnsupportedProviderError} from '../errors';

export default class ProperScopePolicy extends AuthenticationStrategy {
    constructor(scopesProvider) {
        super(null);
        if (!(scopesProvider instanceof ScopesProvider)) {
            throw new UnsupportedProviderError(ScopesProvider.name)
        }
        this.scopesProvider = scopesProvider;
    }

    check(cb) {
        return this.scopesProvider.checkScopes;
    }

}