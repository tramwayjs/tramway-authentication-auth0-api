import AccessTokenPolicy from './AccessTokenPolicy';
import ProperScopePolicy from './ProperScopePolicy';
import CompositePolicy from './CompositePolicy';

export {AccessTokenPolicy, ProperScopePolicy, CompositePolicy};