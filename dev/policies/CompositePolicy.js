import AccessTokenPolicy from './AccessTokenPolicy';
import ProperScopePolicy from './ProperScopePolicy';

import { policies } from 'tramway-core-router';
let { AuthenticationStrategy } = policies;

export default class CompositePolicy extends AuthenticationStrategy {
    constructor(policies = []) {
        super(null);
        this.policies = [];
        policies.forEach((policy) => this.add(policy));
    }

    add(policy) {
        if (policy instanceof AuthenticationStrategy) {
            this.policies.push(policy);
        }
        return this;
    }

    check(cb) {
        return this.policies.map((policy) => policy.check(cb));
    }
}