import jwtAuthz from 'express-jwt-authz';

export default class ScopesProvider {
    constructor(scopes) {
        this.checkScopes = jwtAuthz(scopes);
    }
}