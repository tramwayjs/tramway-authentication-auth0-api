import jwt from 'express-jwt';
import jwksRsa from 'jwks-rsa';
import {InvalidArgumentError} from '../errors'

export default class JwtProvider {
    constructor(options) {
        const {audience, issuer, secret} = options;

        if (!audience || !issuer || !secret) {
            throw new InvalidArgumentError(['audience', 'issuer', 'secret']);
        }

        const {cache = true, rateLimit = true, jwksRequestsPerMinute = 5, jwksUri} = secret;

        if (!jwksUri) {
            throw new InvalidArgumentError(['jwksUri']);
        }

        this.checkJwt = jwt({
            audience: audience,
            issuer: issuer,
            algorithms: ['RS256'],
            secret: jwksRsa.expressJwtSecret({
                cache: cache,
                rateLimit: rateLimit,
                jwksRequestsPerMinute: jwksRequestsPerMinute,
                jwksUri: jwksUri
            })
        });
    }
}