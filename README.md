Tramway Authentication: Auth0 API is a simple implementation of the Auth0 policies on API routes for the tramway framework. It includes:

1. A PolicyFactory for easily creating policies and adding scopes
2. A Security middleware for the `ExpressServerStrategy` of `tramway-core-router`.

# Installation:
1. `npm install tramway-authentication-auth0-api`

# Documentation

## Recommended Folder Structure
- config
- policies
- routes

## Getting Started

1. [Create a resource server API](https://auth0.com/docs/quickstart/backend/nodejs#create-a-resource-server-api-) in the [APIs section of your Auth0 dashboard](https://manage.auth0.com/#/apis). This library was designed to use *RS256* signing for JWT tokens. All of the implementation under Add API Authorization in the Auth0 docs are handled by this library - including scopes.

2. In your tramway app, import the replacement `Security` middleware and pass it to the `ExpressServerStrategy`. This should be in your `server.js` file.

```
import {Security} from 'tramway-authentication-auth0-api';

let router = new Router(routes, new ExpressServerStrategy(app, Security));
```

3. Create and populate your local `.env` file with the following:

```
AUTH0_AUDIENCE=API_IDENTIFIER
AUTH0_DOMAIN=DOMAIN
```

4. Create a `security.js` file in your config folder and export a JSON config using the data given from Auth0. Leave out the function calls so the file looks as follows (with your settings in the placeholders).

```
require('dotenv').config();

if (!process.env.AUTH0_DOMAIN || !process.env.AUTH0_AUDIENCE) {
  throw 'Make sure you have AUTH0_DOMAIN, and AUTH0_AUDIENCE in your .env file';
}

export default {
    audience: process.env.AUTH0_AUDIENCE,
    issuer: `https://${process.env.AUTH0_DOMAIN}.auth0.com/`,
    secret: {
        jwksUri: `https://${process.env.AUTH0_DOMAIN}.auth0.com/.well-known/jwks.json`
    }
};
```

5. Create a `policies.js` file in the policies folder and accompany it with an index.js for convenience. Here we will create the policies using the `PolicyFactory`. Pass the security configuration from step 2 directly to the factory.

```
import {PolicyFactory} from 'tramway-authentication-auth0-api';
import {security} from '../config';

let policyFactory = new PolicyFactory(security);

export default {
    LOGGED_IN_POLICY: policyFactory.buildBasic(),
    CAN_WRITE_POLICY: policyFactory.buildScoped(['write:hello'])
};
```

6. Import the policies to your `routes.js` file in the routes folder.

```
import policies from '../policies';

let {LOGGED_IN_POLICY, CAN_WRITE_POLICY} = policies;
```

You can then use the policy on a route using the policy key. 

```
"policy": CAN_WRITE_POLICY
```

## PolicyFactory

The `PolicyFactory` simplifies the creation of policies by streamlining prerequisites.

```
import {PolicyFactory} from 'tramway-authentication-auth0-api;
```

The constructor takes a config object of the following form:

```
{
    audience: 'http://localhost:8081',
    issuer: `https://YOURAPPNAME.auth0.com/`,
    secret: {
        jwksUri: `https://YOURAPPNAME.auth0.com/.well-known/jwks.json`
    }
};
```

Other settings in the secret can be overrided as per the jwt api. The defaults are:

```
cache: true,
rateLimit: true,
jwksRequestsPerMinute: 5,
```

| Function | Parameters | Default | Return | Description |
| --- | --- | --- | --- | --- |
| buildBasic | | | `AccessTokenPolicy` | Performs basic authentication based on whether the Bearer token is valid. |
| buildScoped | scopes: string[] | | `CompositePolicy` | Performs basic authentication, then checks if the proper scope is applied. |

## Providers

```
import {providers} from 'tramway-authentication-auth0-api';
{JwtProvider, ScopesProvider} = providers;
```

### JwtProvider
Handles interaction with the jwt libraries to create the checkToken middleware. 

The constructor takes a config object of the following form:

```
{
    audience: 'http://localhost:8081',
    issuer: `https://YOURAPPNAME.auth0.com/`,
    secret: {
        jwksUri: `https://YOURAPPNAME.auth0.com/.well-known/jwks.json`
    }
};
```

The `checkToken` method is generated when the object is constructed and returns an Express RouteHandler callback.

### ScopesProvider
Handles interaction with `express-jwt-authz` to create the checkScopes middleware.

The constructor takes an array of strings representing the scopes you can set up in the Auth0 dashboard.

## Policies 

### AccessTokenPolicy

The `AccessTokenPolicy` handles the validity of the a Bearer token on a given Route. It expects a `JwtProvider` as its constructor argument and `check` returns the middleware from `JwtProvider.checkToken`.

### ProperScopePolicy

The `ProperScopePolicy` handles the validity of the token's scope. It expects a `ScopesProvider` as its constructor argument and `check` returns the middleware from `ScopesProvider.checkScopes`.

### CompositePolicy

The `CompositePolicy` allows for multiple policies to be put together to form a sequenced middleware. It takes an array of `AuthenticationStrategy` objects or alternatively lets you add `AuthenticationStrategy` objects via its `add` function. Its `check` returns an array of the middleware that is added to it.

## Errors
The following errors will be thrown in the following cases:

| Error | Condition |
| --- | --- |
| `InvalidArgumentError` | The configuration passed to `JwtProvider` is wrong. Be sure to follow the aforementioned guidelines. |
| `UnsupportedProviderError` | The provider set in one of the policies doesn't match its expected provider type. |